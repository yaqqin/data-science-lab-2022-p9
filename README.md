# DS Lab 2022 P2 Cyclone

## Description
This is the gitlab repository of the cyclone intensity prediction project (P2).

## Supervisors
* Prof. Dr. Sebastian Schemm sebastian.schemm@env.ethz.ch
* Dr. Michael Armand Sprenger michael.sprenger@env.ethz.ch
* Prof. Dr. Ce Zhang ce.zhang@inf.ethz.ch

## Authors
* Yaqi Qin yaqqin@student.ethz.ch
* Feichi Lu feiclu@student.ethz.ch
* Tianyang Xu tianyxu@student.ethz.ch
